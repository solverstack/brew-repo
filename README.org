#+TITLE: Solverstack's Brew repository

On a MacOSX system install Homebrew
#+begin_src sh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
#+end_src

Then one can install a package as follows
#+begin_src sh
git clone https://gitlab.inria.fr/solverstack/brew-repo.git
brew install --build-from-source ./brew-repo/chameleon.rb
#+end_src

* starpu
  Default version: with openmpi.
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/starpu.rb
  #+end_example

* chameleon
  Default version: with openblas, openmpi, starpu. Note you will have
  to install starpu from source first
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/starpu.rb
  brew install -v --cc=clang --build-from-source ./brew-repo/chameleon.rb
  #+end_example

* chameleon-openmp
  Specific version: with openmp, openblas.
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/chameleon-openmp.rb
  #+end_example

* pastix
  Default version: with openmpi, openblas, hwloc and scotch (int32).
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/pastix.rb
  #+end_example

* qr_mumps
  Default version: with amd, scotch, metis, openblas.
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/qr_mumps.rb
  #+end_example
  Notice that starpu is an optional dependency, use ~--with-starpu~ to
  enable build with starpu.

* fabulous
  Default version: with openblas.
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/fabulous.rb
  #+end_example
  Notice that chameleon is an optional dependency, use
  ~--with-chameleon~ to enable build with chameleon (not supported by
  the last release 1.1.0 but will be in next releases).

* composyx
  Default version: with blaspp and lapackpp (using openblas), openmpi,
  arpack, pastix, fabulous, eigen.
  #+begin_example
  brew install -v --cc=clang --build-from-source ./brew-repo/blaspp.rb
  brew install -v --cc=clang --build-from-source ./brew-repo/lapackpp.rb
  brew install -v --cc=clang --build-from-source ./brew-repo/arpackng.rb
  brew install -v --cc=clang --build-from-source ./brew-repo/fabulous.rb
  brew install -v --cc=clang --build-from-source ./brew-repo/pastix.rb
  brew install llvm
  brew install -v --cc=llvm_clang --build-from-source ./brew-repo/composyx.rb
  #+end_example

* Creating a package example
  We have a MacOSX Catalina virtual machine on
  ci.inria.fr. Requirement: be member of [[https://ci.inria.fr/project/hpclib/show][project hpclib]] on
  ci.inria.fr, ask [[mailto:florent.pruvost@inria.fr][Florent Pruvost]].

  #+begin_src sh
  ssh ci@hpclib-macos.ci
  #+end_src

  Initialisation of the package
  #+begin_src sh
  brew create https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz
  #+end_src

  Edit the package
  #+begin_example
  sudo vim /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core/Formula/chameleon.rb

  class Chameleon < Formula
    desc ""
    homepage ""
    url "https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz"
    sha256 "e64d0438dfaf5effb3740e53f3ab017d12744b85a138b2ef702a81df559126df"
    license "CeCILL-C"

    depends_on "cmake" => :build
    depends_on "openblas"

    def install
      system "cmake", "-S", ".", "-B", "build", "-DCHAMELEON_SCHED=OPENMP", "-DBUILD_SHARED_LIBS=ON", *std_cmake_args
      system "cmake", "--build", "build"
      system "cmake", "--install", "build"
    end

    test do
      (testpath/"test.c").write <<~EOS
        #include <stdio.h>
        #include <stdlib.h>
        #include <chameleon.h>

        int main(int argc, char* argv[]) {
          int major, minor, patch;
          CHAMELEON_Version(&major, &minor, &patch);
          printf("CHAMELEON_Version: %d.%d.%d\n", major, minor, patch);
          return 0;
        }
      EOS

      system ENV.cc, "test.c", "-I#{include}", "-L#{lib}", "-lchameleon",
                     "-o", "test"
      system "./test"
    end
  end
  #+end_example

  Test
  #+begin_src sh
  brew install --build-from-source chameleon
  #+end_src

  Then later and elsewhere one can install the package with the .rb file
  #+begin_src
  brew install --build-from-source chameleon.rb
  #+end_src

  Note: be sure to be able to create files in /usr/local
  #+begin_src
  sudo chown -R $USER /usr/local/*
  #+end_src
