class Arpackng < Formula
  desc "Routines to solve large scale eigenvalue problems"
  homepage "https://github.com/opencollab/arpack-ng"
  url "https://github.com/opencollab/arpack-ng/archive/3.9.0.tar.gz"
  sha256 "24f2a2b259992d3c797d80f626878aa8e2ed5009d549dad57854bbcfb95e1ed0"
  license "BSD-3-Clause"
  head "https://github.com/opencollab/arpack-ng.git", branch: "master"

  depends_on "cmake"  => :build
  depends_on "pkg-config" => :build

  depends_on "eigen"
  depends_on "gcc" # for gfortran
  depends_on "open-mpi"
  depends_on "openblas"
  depends_on "blaspp"
  depends_on "lapackpp"

  def install
    args = [*std_cmake_args,
            "-DICB=ON",
            "-DBUILD_SHARED_LIBS=ON"]

    system "cmake", "-S", ".", "-B", "build", *args
    system "cmake", "--build", "build"
    system "cmake", "--install", "build"
  end
end
