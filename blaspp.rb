class Blaspp < Formula
  desc "The Basic Linear Algebra Subprograms (BLAS) in C++"
  homepage "https://github.com/icl-utk-edu/blaspp"
  url "https://github.com/icl-utk-edu/blaspp/releases/download/v2024.05.31/blaspp-2024.05.31.tar.gz"
  sha256 "24f325d2e1c2cc4275324bd88406555688379480877d19553656a0328287927a"
  license "BSD"

  depends_on "cmake" => :build
  depends_on "openblas"

  def install
    system "cmake", "-S", ".", "-B", "build", "-DBUILD_SHARED_LIBS=ON", *std_cmake_args
    system "cmake", "--build", "build"
    system "cmake", "--install", "build"
  end
end
