class ChameleonOpenmp < Formula
  desc "Dense linear algebra subroutines for heterogeneous and distributed architectures"
  homepage "https://gitlab.inria.fr/solverstack/chameleon"
  url "https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz"
  sha256 "2725d2d2a9885e619e0c8d41306b9b9dc6d5df635b710cf8d077a14803ea26cd"
  license "CeCILL-C"

  depends_on "cmake" => :build
  depends_on "openblas"

  def install
    system "cmake", "-S", ".", "-B", "build", "-DCHAMELEON_SCHED=OPENMP", "-DBUILD_SHARED_LIBS=ON", *std_cmake_args
    system "cmake", "--build", "build"
    system "cmake", "--install", "build"
  end

  test do
    (testpath/"test.c").write <<~EOS
      #include <stdio.h>
      #include <stdlib.h>
      #include <chameleon.h>

      int main(int argc, char* argv[]) {
        int major, minor, patch;
        CHAMELEON_Version(&major, &minor, &patch);
        printf("CHAMELEON_Version: %d.%d.%d\n", major, minor, patch);
        return 0;
      }
    EOS

    system ENV.cc, "test.c", "-I#{include}", "-L#{lib}", "-lchameleon",
                   "-o", "test"
    system "./test"
  end
end
