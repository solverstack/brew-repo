class Composyx < Formula
  desc "Massively Parallel Hybrid Solver in C++"
  homepage "https://gitlab.inria.fr/composyx/composyx"
  url "https://gitlab.inria.fr/api/v4/projects/52455/packages/generic/source/v1.0.1/composyx-1.0.1.tar.gz"
  sha256 "d97936e3b297fde435c165cbe29cb39e5d88ae368be451b1c45b8ee51486782c"
  head "https://gitlab.inria.fr/composyx/composyx.git"
  license "CeCILL-C"

  depends_on "blaspp"
  depends_on "lapackpp"
  depends_on "arpackng"
  depends_on "openmpi"
  depends_on "hwloc"
  depends_on "pastix"
  #depends_on "fabulous"
  #depends_on "eigen"
  depends_on "gcc"    => :build    # GNU Fortran is now provided as part of GCC
  depends_on "cmake"  => :build

  def install
    args = ["-DCMAKE_INSTALL_PREFIX=#{prefix}",
            "-DBUILD_SHARED_LIBS=ON",
            "-DCMAKE_EXE_LINKER_FLAGS=-L/usr/local/opt/gcc/lib/gcc/current",
            "-DCOMPOSYX_COMPILE_EXAMPLES=ON",
            "-DCOMPOSYX_COMPILE_TESTS=OFF",
            "-DCOMPOSYX_USE_ARMADILLO=OFF",
            "-DCOMPOSYX_USE_ARPACK=ON",
            "-DCOMPOSYX_USE_EIGEN=OFF",
            "-DCOMPOSYX_USE_FABULOUS=OFF",
            "-DCOMPOSYX_USE_MUMPS=OFF",
            "-DCOMPOSYX_USE_PASTIX=ON"
           ]

    mkdir "build" do
      system "cmake", "..", *args
      system "make"
      system "make", "install"
    end
  end

  test do
    system "mpirun -n 8 --oversubscribe #{prefix}/bin/composyx_driver_cg -P build/matrices/partitioned/ -N 8 -p AS"
  end
end
