class Fabulous < Formula
  desc "Fast Accurate Block Linear krylOv Solver, a.k.a IB-BGMRES-DR"
  homepage "https://gitlab.inria.fr/solverstack/fabulous"
  url "https://gitlab.inria.fr/api/v4/projects/2083/packages/generic/source/v1.1.4/fabulous-1.1.4.tar.gz"
  sha256 "bdfad5addb6df40b811bc076eacf71555f6a6291fe043e8eb9f9b0a4ee9720c5"
  license "CeCILL-C"

  depends_on "cmake"  => :build
  depends_on "gcc"    => :build    # GNU Fortran is now provided as part of GCC
  depends_on "libomp" => :build
  depends_on "openblas"
  depends_on "chameleon" => :optional # Enable Chameleon

  def install
    args = [*std_cmake_args,
            "-DFABULOUS_BUILD_C_API=ON",
            "-DFABULOUS_BUILD_Fortran_API=ON",
            "-DFABULOUS_BUILD_EXAMPLES=ON",
            "-DBUILD_SHARED_LIBS=ON"]
    args += ["-DFABULOUS_USE_CHAMELEON=ON"] if build.with? "chameleon"

    system "cmake", "-S", ".", "-B", "build", *args
    system "cmake", "--build", "build"
    system "cmake", "--install", "build"
  end

  test do
    system "#{prefix}/lib/fabulous/examples/example_api"
  end
end
