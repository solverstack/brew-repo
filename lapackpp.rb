class Lapackpp < Formula
  desc "The Linear Algebra PACKage (LAPACK) in C++"
  homepage "https://github.com/icl-utk-edu/lapackpp"
  url "https://github.com/icl-utk-edu/lapackpp/releases/download/v2024.05.31/lapackpp-2024.05.31.tar.gz"
  sha256 "093646d492a4c2c6b4d7001effb559c80da7fa31fd5ba517a6d686ca8c78cd99"
  license "BSD"

  depends_on "cmake" => :build
  depends_on "openblas"

  def install
    system "cmake", "-S", ".", "-B", "build", "-DBUILD_SHARED_LIBS=ON", *std_cmake_args
    system "cmake", "--build", "build"
    system "cmake", "--install", "build"
  end
end
