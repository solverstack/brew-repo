###
#
#  @file qr_mumps.rb
#  @copyright 2021 Inria, CNRS, INPT
#
#  @brief Homebrew formula for Qr_Mumps 3.*
#
#  @version 3.0.2
#  @author Florent Pruvost
#  @author Alfredo Buttari
#  @date 2021-05-27
#
###
class QrMumps < Formula
  desc "Parallel solver for sparse linear systems based on direct methods QR and Cholesky"
  homepage "http://buttari.perso.enseeiht.fr/qr_mumps/"
  url "https://gitlab.com/qr_mumps/qr_mumps/-/archive/3.1/qr_mumps-3.1.tar.gz"
  sha256 "6e39dbfa1e6ad3730b006c8953a43cc6da3dfc91f00edeb68a641d364703b773"
  license "LGPL"

  depends_on "openblas"
  depends_on "suite-sparse"
  depends_on "scotch"
  depends_on "metis"
  depends_on "starpu" => :optional # Enable STARPU runtime
  depends_on "gcc"    => :build    # GNU Fortran is now provided as part of GCC
  depends_on "cmake"  => :build

  def install
    args = ["-DCMAKE_INSTALL_PREFIX=#{prefix}",
            "-DBUILD_SHARED_LIBS=ON",
            "-DQRM_ORDERING_AMD=ON",
            "-DQRM_ORDERING_SCOTCH=ON",
            "-DQRM_ORDERING_METIS=ON"]
    args += ["-DQRM_WITH_STARPU=ON"] if build.with? "starpu"
    mkdir "build" do
      system "cmake", "..", *args
      system "make"
      system "make", "install"
    end
  end

  test do
    system "#{prefix}/bin/dqrm_least_squares_basic"
  end
end
